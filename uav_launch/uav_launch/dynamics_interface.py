"""Provides interface and storage for the dynamics

Classes:
    DynamicsData: Stores all of the data for the simulation and functions for updating the data

Functions:
    convert_mav_state_to_msg(): Converts the state to a message using the DynamicsData
    publish_state_wrench(): Publishes the state, wrench and combined messages
                            Returns the combined state and wrench data


"""

import numpy as np
import rclpy
from geometry_msgs.msg import Vector3Stamped, WrenchStamped
from mav_sim.chap3.mav_dynamics import DynamicState, ForceMoments
from mav_sim.chap4.mav_dynamics import MavDynamics
from mav_sim.message_types.msg_delta import MsgDelta
from mav_sim.tools import types
from mav_sim.tools.rotations import Euler2Quaternion
from rclpy.node import Node, Publisher
from rclpy.time import Duration, Time
from std_srvs.srv import Empty
from tf2_ros import TransformException
from tf2_ros.buffer import Buffer
from tf2_ros.transform_listener import TransformListener
from uav_interfaces.msg import (
    ControlSurfaceCommands,
    UavState,
    UavStateWrench,
    WindVector,
)
from uav_interfaces.srv import SetUavState
from uav_launch.conversions import convert_mav_state_to_msg as convert_state_to_msg


class DynamicsData:
    """Stores all of the data for the simulation and includes functions to update the data

        Functions for updating the data via ROS include:
            command_callback(): Stores the flap/thrust commands to the aircraft
            wind_callback(): Stores the wind data
            reset_state_callback(): Resets the uav state
            set_state_callback(): Sets the uav initial state to the desired value
            toggle_callback(): Toggles the dynamics to be running or not. If running, it pauses. If paused,
                               unpauses

        Functions that can be used to publish data to ROS
            publish_state_wrench(): Publishes a state only, wrench only, and a combined message

        Function for updating the state via a dynamic simulation:
            update_state(): Updates the state via a simulation step if the data.executing variable is true
                    Returns whether or not an update was made


    Attributes:
        ts(float): Simulation time step (period)
        executing(bool): Flag indicating whether or not the state is to be updated via dynamics
        fm(ForcesMoments): The forces/moments being applied to the aircraft
        mav(MavDynamics): Dynamic variables and functions for the uav
        time_latest(Time): Access to the latest time for which the state was published
        tf_buffer(Buffer): Stores a buffer of transform data
        command(MsgDelta): The commanded flap angles and thrust
        wind(types.WindVector): Public interface for the wind vectors (steady state and gust)

        _state_init(DynamicState): The state of the aircraft
        _time_latest(Time): Stores the latest time for which the state was published
        _tf_listener(TransoformListener): Listens to the transform data
        _wind(types.WindVector): The wind vectors (steady state and gust)
        _wind_ss(Vector3Stamped): The steady state wind vector (ned frame)
        _wind_gust(Vector3Stamped): The gust vector (body frame)
    """
    def __init__(self, ts: float, time_latest: Time, node: Node) -> None:
        """Create the dynamics variables

        Inputs:
            ts: Simulation time step
            node: Node used to determine initial time and tf data
        """
        self.ts = ts # Simulation time step
        self.executing: bool = False # Flag inidicating whether or not the state is executing

        # MAV state and inputs data
        self._state_init: DynamicState = DynamicState()
        self.fm = ForceMoments(force_moment=np.zeros([6,1]))
        self.mav = MavDynamics(self.ts, self._state_init)
        self._time_latest: Time = time_latest # Stores the latest time seen

        # Create the transform listener
        self.tf_buffer = Buffer()
        self._tf_listener = TransformListener(self.tf_buffer, node)

        # Create storage for data passed in from node subscribers
        self.command = MsgDelta()
        self._wind: types.WindVector = np.zeros((6,1))
        self._wind_ss = Vector3Stamped() # Stores the steady state wind, initialized to zero with the ned frame
        self._wind_ss.header.frame_id = "ned"
        self._wind_ss.header.stamp = self._time_latest.to_msg()
        self._wind_gust = Vector3Stamped() # Stores the wind gust, initialized to zero with the body frame
        self._wind_gust.header.frame_id = "body"
        self._wind_gust.header.stamp = self._time_latest.to_msg()

    @property
    def wind(self) -> types.WindVector:
        """Getter for the wind
        """
        return self._wind

    @property
    def time_latest(self) -> Time:
        """Returns the time corresponding to the latest state"""
        return self._time_latest

    def command_callback(self, msg: ControlSurfaceCommands) -> None:
        '''Stores the control surface commands for use by the dynamics

        Args:
            msg: The flap and thrust commands for the aircraft
        '''
        self.command.elevator = msg.elevator
        self.command.aileron = msg.aileron
        self.command.rudder = msg.rudder
        self.command.throttle = msg.throttle

    def wind_callback(self, msg: WindVector) -> None:
        '''Stores the most recent wind vector information

        Args:
            msg: The steady state and gust vectors
        '''
        self._wind_ss = msg.steady_state
        self._wind_gust = msg.gust

    def extract_wind(self) -> None:
        '''Takes the steady state and gust wind values, converts them to the correct frame, and stores
           them in the wind structure
        '''
        # Transform steady state vector
        if self._wind_ss.header.frame_id == "ned":
            wind_ss_trans = self._wind_ss
        else:
            try:
                #TODO: Fix the transform code below
                rclpy.logger.get_logger("DynamicsData").error("Transforms not yet implemented")
                rclpy.logger.get_logger("DynamicsData").warn("Wind_ss frame = " + self._wind_ss.header.frame_id)
                wind_ss_trans = self.tf_buffer.transform(object_stamped=self._wind_ss, target_frame="ned")
            except TransformException:
                rclpy.logger.get_logger("DynamicsData").warn(
                    'could not transform steady stead wind from ' + self._wind_ss.header.frame_id + ' to ned')
                wind_ss_trans = Vector3Stamped()

        # Transform gust vector
        if self._wind_gust.header.frame_id == "body":
            wind_gust_trans = self._wind_gust
        else:
            try:
                #TODO: Fix the transform code below
                rclpy.logger.get_logger("DynamicsData").error("Transforms not yet implemented")
                rclpy.logger.get_logger("DynamicsData").warn("Wind_gust frame = " + self._wind_gust.header.frame_id)
                wind_gust_trans = self.tf_buffer.transform(object_stamped=self._wind_gust, target_frame="body")
            except TransformException:
                rclpy.logger.get_logger("DynamicsData").get_logger().warn(
                    'could not transform gust wind from ' + self._wind_gust.header.frame_id + ' to body')
                wind_gust_trans = Vector3Stamped()

        # Extract wind variables
        self._wind[0] = wind_ss_trans.vector.x
        self._wind[1] = wind_ss_trans.vector.y
        self._wind[2] = wind_ss_trans.vector.z
        self._wind[3] = wind_gust_trans.vector.x
        self._wind[4] = wind_gust_trans.vector.y
        self._wind[5] = wind_gust_trans.vector.z

    def reset_state_callback(self, req: Empty.Request, res: Empty.Response) -> Empty.Response: #pylint: disable=unused-argument
        """Resets the state and inputs to the default. Note that both the request and response are empty

        Args:
            req: Empty value - ignored. The reciept of the callback simply implies that a request is received to reset state
            res: The respose - again empty
        """
        self.reset_state()
        return res

    def reset_state(self) -> None:
        """Resets the state and inputs to the default."""
        self.mav = MavDynamics(self.ts, self._state_init)
        self.command = MsgDelta()
        self._wind = np.zeros((6,1))

    def set_state_callback(self, req: SetUavState.Request, res: SetUavState.Response) -> SetUavState.Response:
        """Sets the uav initial state to the desired value

        Args:
            req: The desired state to use as the initial state
            res: The resulting state used (ensures that the frames, etc, are defined appropriately)
        """
        # Check position frame
        if req.desired_state.pose.header.frame_id != "ned":
            rclpy.logger.get_logger("DynamicsData").get_logger().warn(
                "SetState service request position does not have ned frame, setting assuming ned frame")

        # Set position
        self._state_init.north = float(req.desired_state.pose.pose.position.x)
        self._state_init.east = req.desired_state.pose.pose.position.y
        self._state_init.down = req.desired_state.pose.pose.position.z

        # Set orientation
        quat = Euler2Quaternion(phi=req.desired_state.phi, theta=req.desired_state.theta, psi=req.desired_state.psi)
        self._state_init.e0 = quat.item(0)
        self._state_init.e1 = quat.item(1)
        self._state_init.e2 = quat.item(2)
        self._state_init.e3 = quat.item(3)

        # Check twist frame
        if req.desired_state.twist.header.frame_id != "body":
            rclpy.logger.get_logger("DynamicsData").warn(
                "SetState service request velocities (twist) does not have body frame, setting assuming body frame")

        # Set linear velocities
        self._state_init.u = req.desired_state.twist.twist.linear.x
        self._state_init.v = req.desired_state.twist.twist.linear.y
        self._state_init.w = req.desired_state.twist.twist.linear.z

        # Set rotational velocities
        self._state_init.p = req.desired_state.twist.twist.angular.x
        self._state_init.q = req.desired_state.twist.twist.angular.y
        self._state_init.r = req.desired_state.twist.twist.angular.z

        # Check other variables and set warning message
        if  req.desired_state.pose.pose.orientation.x != 0. or \
            req.desired_state.pose.pose.orientation.y != 0. or \
            req.desired_state.pose.pose.orientation.z != 0. or \
            req.desired_state.pose.pose.orientation.w != 1. or \
            req.desired_state.gyro_bias.vector.x != 0. or \
            req.desired_state.gyro_bias.vector.y != 0. or \
            req.desired_state.gyro_bias.vector.z != 0. or \
            req.desired_state.v_a != 0. or \
            req.desired_state.v_g != 0. or \
            req.desired_state.alpha != 0. or \
            req.desired_state.beta != 0. or \
            req.desired_state.gamma != 0. or \
            req.desired_state.chi != 0. :
            rclpy.logger.get_logger("DynamicsData").warn(
                "SetState Service only uses position, euler angles, and twist. Another variable was seen to be non-zero")

        # Reset the state and populate the resulting state
        self.reset_state()
        res.resulting_state = convert_mav_state_to_msg(self, 1.)
        return res

    def toggle_callback(self, req: Empty.Request, res: Empty.Response) -> Empty.Response: #pylint: disable=unused-argument
        """Toggles whether or not the state is updating"""
        self.executing = not self.executing
        return res

    def update_state(self, delta: MsgDelta, time_step: float) -> bool:
        """Takes a single simulation step for the mav. The internal state of data.mav is updated.

        Args:
            delta: The command from the autopilot
            time_step: The size of the timestep for simulation

        Returns:
            True if state updated, false otherwise
        """
        if self.executing:
            self.mav.update(delta=delta, wind=self.wind, time_step=time_step)
            self._time_latest += Duration(seconds=time_step)
        return self.executing

def convert_mav_state_to_msg(data: DynamicsData, velocity_scale: float) -> UavState:
    ''' Extracts the state from the mav and stores it in a UavState message

    Args:
        data: The data to be used to convert the state to a message
        velocity_scale: Scale on the velocities. This allows the velocities to easily be zeroed out when
                        the sim is not moving
    Returns:
        The current state in the mav
    '''
    state = data.mav.get_struct_state()
    return convert_state_to_msg(time=data.time_latest, msg_state=data.mav.true_state,
                                dyn_state=state, velocity_scale=velocity_scale)

def publish_state_wrench(data: DynamicsData, pub_state: Publisher,
        pub_wrench: Publisher, pub_combined: Publisher) -> UavStateWrench:
    """Publishes the state, wrench, and combined messages

    Inputs:
        data: The dynamics data storing the mav data
        pub_state: Publisher of the UavState state message
        pub_wrench: Publisher of the WrenchStamped message
        pub_combined: Publisher of the UavStateWrench message

    Returns:
        combined state/wrench message
    """
    # Publish the updated state
    state = convert_mav_state_to_msg(data=data, velocity_scale=1.0)
    pub_state.publish(state)

    # Publish the forces and moments vector
    wrench_struct = data.mav.get_fm_struct()
    wrench = WrenchStamped()
    wrench.header.stamp = data.time_latest.to_msg()
    wrench.header.frame_id = 'body'
    wrench.wrench.force.x = wrench_struct.fx
    wrench.wrench.force.y = wrench_struct.fy
    wrench.wrench.force.z = wrench_struct.fz
    wrench.wrench.torque.x = wrench_struct.l
    wrench.wrench.torque.y = wrench_struct.m
    wrench.wrench.torque.z = wrench_struct.n
    pub_wrench.publish(wrench)

    # Publish the combined state and wrench
    state_wrench = UavStateWrench()
    state_wrench.state = state
    state_wrench.wrench = wrench
    pub_combined.publish(state_wrench)
    return state_wrench
