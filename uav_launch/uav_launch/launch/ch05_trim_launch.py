"""Runs the `transforms`, `ch04_dynamic_mav`, `ch04_wind`, and `ch05_trim_traj_node` nodes as well as
opens rviz2, rqt_reconfigure, and `uav_plotter`. `rqt_robot_monitor` is also opened to allow easy monitoring
of the status of different components. Again, keep in mind the scale of rviz.

Before the vehicle will begin moving, you must click the "Start" button on the "Control Panel" in rviz2. You
can then pause the sim by clicking the button again.

New trim trajectories can be calculated and commanded through rqt_reconfigure.
* In rqt_reconfigure, click the *Refresh* button until *trim_traj_gen* appears in the left-hand menu
* Click on *trim_traj_gen*
* Modify parameters as desired
  * `va_trim`: Airspeed ($V_a$) for the trim trajectory
  * `gamma_trim`: Flight path angle ($\gamma$) for the trim trajectory
  * `ts`: Time in seconds between publication of the trim trajectory
* Once the parameters have been modified, the trim trajectory node will calculate the trim trajectory and
reset the mav state by calling the `set_state` service with the updated trim state

"""
import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription  # type: ignore
from launch_ros.actions import Node


def generate_launch_description()-> LaunchDescription:
    """Launch chapter 5 demonstration
    """
    # Create the package directory
    uav_launch_dir = get_package_share_directory('uav_launch')

    # Define the urdf file for visualizing the uav
    urdf_file_name = 'fixed_wing_uav.urdf'
    urdf = os.path.join(
        get_package_share_directory('uav_launch'),
        'urdf',
        urdf_file_name)

    # Flag for enabling/disabling use of simulation time instead of wall clock
    use_sim_time = True

    return LaunchDescription([
        ################ Dynamics and kinematics #####################################
        Node(
            package='uav_launch',
            executable='transforms',
            name='transforms',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            arguments=[urdf],
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch04_dynamic_mav',
            name='dynamic_mav',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch04_wind',
            name='wind',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# UAV Control Software #######################################
        Node(
            package='uav_launch',
            executable='ch05_trim',
            name='trim_traj_gen',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# Tools for Interacting with the Sim #########################
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', [os.path.join(uav_launch_dir, 'ch04_dynamics.rviz')]],
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='rqt_reconfigure',
            executable='rqt_reconfigure',
            name='rqt_reconfigure',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='rqt_robot_monitor',
            executable='rqt_robot_monitor',
            name='rqt_robot_monitor',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='uav_launch',
            executable='diagnotic_aggregator',
            name='diagnotic_aggregator',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='uav_plotter',
            name='uav_plotter',
            parameters=[{
                'use_sim_time': True,
                't_horizon': 100.,
                'plot_sensors': False
            }]
        )
    ])