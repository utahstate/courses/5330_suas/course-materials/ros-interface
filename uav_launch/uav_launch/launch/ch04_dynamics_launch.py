"""Runs the `transforms`, `ch04_dynamic_mav`, and `ch04_wind` nodes as well as opens rviz2, rqt_publisher,
and `uav_plotter`. Again, keep in mind the scale of rviz.

Before the vehicle will begin moving, you must click the "Start" button on the "Control Panel" in rviz2. You
can then pause the sim by clicking the button again.

As in Chapter 3, the rqt_publisher can be used to publish commands to the UAV using the */command* topic. As
in Chapter 3, the UAV state can also be reset using either the "Reset" button in the control panel or through
a service call.

An additional service exists to set the desired state.
* In a new terminal, source the install directory and type the command `rqt` to bring up the rqt gui
* Select the *Service Caller* tool from the *Plugins->Services* menu
* Select ***/set_state*** from the dropdown menu
* Populate the desired state
  * The service only uses position, euler angles, and twist. Modifying any other variable will be ignored
  except for a warning output to the console.
  * It is assumed that the position is defined in the *ned* frame and that the twist is defined in the *body*
  frame. If an incorrect frame_id is seen, a warning will be pushed to the terminal. The variables will be used
  as if they had the correct frame_id.
* Click the *Call* button
"""
import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription  # type: ignore
from launch_ros.actions import Node


def generate_launch_description() -> LaunchDescription:
    """Launch demonstration for chapter 4
    """
    # Create the package directory
    uav_launch_dir = get_package_share_directory('uav_launch')

    # Define the urdf file for visualizing the uav
    urdf_file_name = 'fixed_wing_uav.urdf'
    urdf = os.path.join(
        get_package_share_directory('uav_launch'),
        'urdf',
        urdf_file_name)

    # Flag for enabling/disabling use of simulation time instead of wall clock
    use_sim_time = True

    return LaunchDescription([
        ################ Dynamics and kinematics #####################################
        Node(
            package='uav_launch',
            executable='transforms',
            name='transforms',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            arguments=[urdf],
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch04_dynamic_mav',
            name='dynamic_mav',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch04_wind',
            name='wind',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# Tools for Interacting with the Sim #########################
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', [os.path.join(uav_launch_dir, 'ch04_dynamics.rviz')]],
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='rqt_publisher',
            executable='rqt_publisher',
            name='rqt_publisher',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='uav_launch',
            executable='uav_plotter',
            name='uav_plotter',
            parameters=[{
                'use_sim_time': True,
                't_horizon': 100.,
                'plot_sensors': False
            }]
        )
    ])
