"""Runs the `transforms` node as well as opens rviz2.

To change the transforms, use the `State panel` with rviz.

By default, only one frame is shown in Rviz, *ned* (north-est-down). Additional frames can be visualized by opening the *Displays* menu on the left and selecting the desired frames under *TF->Frames*.

Also note the scale in rviz. Each grid square is 100 x 100 $m^2$, so correspondingly large values for position must be chosen to see the difference in position.

The axes are differentiated by color
* Red: x-axis  (body frame - out the nose of the aircraft, ned frame - north)
* Green: y-axis (body frame - out the right wing of the aircraft, ned frame - east)
* Blue: z-axis (body frame - out the belly of the aircraft, ned frame - down)
"""
import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription  # type: ignore
from launch_ros.actions import Node


def generate_launch_description() -> LaunchDescription:
    """Launch chapter 2 demonstration
    """
    # Create the package directory
    uav_launch_dir = get_package_share_directory('uav_launch')

    # Define the urdf file for visualizing the uav
    urdf_file_name = 'fixed_wing_uav.urdf'
    urdf = os.path.join(
        get_package_share_directory('uav_launch'),
        'urdf',
        urdf_file_name)

    return LaunchDescription([
        ################ Dynamics and kinematics #####################################
        Node(
            package='uav_launch',
            executable='transforms',
            name='transforms',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            arguments=[urdf],
            parameters=[{
                'use_sim_time': False
            }]),

        ################# Tools for Interacting with the Sim #########################
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', [os.path.join(uav_launch_dir, 'ch02_transforms.rviz')]],
            parameters=[{
                'use_sim_time': False
            }]
        )
    ])