"""Runs the `transforms` and `ch03_kinematic_mav` nodes for publishing the uav state and transforms.
It also opens rviz2, rqt_publisher, and `uav_plotter` for interacting with the sim. The `robot_state_publisher`
is used to create the uav visualization. The rqt_publisher can be used to publish the forces and moments acting
upon the UAV. Do the following in the rqt_publisher.
* Select the */forces_moments* topic from the "topic" drop down list
* Select a good publishing frequency (1 hz will do)
* Click the *+* button to add the publisher
* Select values for the force and torque vectors (i.e., a wrench)
* Click the checkbox to begin publishing

Keep in mind the rviz scale of 100 x 100 $m^2$. A small force will not appear to move the UAV much. However, a
small torque will.

There is no need to restart the entire simulation to rerun a different wrench combination. Simply click the
"Reset" button on the control panel in rviz2 and the UAV will move back to the starting position.

This reset works via a service call to set the UAV back to the starting position. This can be called from the
rqt gui by doing the following.
* Uncheck the publishing of the wrench message in rqt_publisher
* In a new terminal, source the install directory and type the command `rqt` to bring up the rqt gui
* Select the *Service Caller* tool from the *Plugins->Services* menu
* Select ***/reset_state*** from the dropdown menu
* Click the *Call* button
"""

import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription  # type: ignore
from launch_ros.actions import Node


def generate_launch_description() -> LaunchDescription:
    """Launches chapter 3 demonstration"""
    # Create the package directory
    uav_launch_dir = get_package_share_directory('uav_launch')

    # Define the urdf file for visualizing the uav
    urdf_file_name = 'fixed_wing_uav.urdf'
    urdf = os.path.join(
        get_package_share_directory('uav_launch'),
        'urdf',
        urdf_file_name)

    return LaunchDescription([
        ################ Dynamics and kinematics #####################################

        Node(
            package='uav_launch',
            executable='transforms',
            name='transforms',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            arguments=[urdf],
            parameters=[{
                'use_sim_time': False
            }]
            ),
        Node(
            package='uav_launch',
            executable='ch03_kinematic_mav',
            name='kinematic_mav',
            parameters=[{
                'use_sim_time': False
            }]
        ),


        ################# Tools for Interacting with the Sim #########################
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', [os.path.join(uav_launch_dir, 'ch03_forces_moments.rviz')]],
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='rqt_publisher',
            executable='rqt_publisher',
            name='rqt_publisher',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='uav_launch',
            executable='uav_plotter',
            name='uav_plotter',
            parameters=[{
                'use_sim_time': True,
                't_horizon': 100.,
                'plot_sensors': False
            }]
        )
    ])
