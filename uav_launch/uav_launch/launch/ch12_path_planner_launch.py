"""Runs the `transforms`, `ch04_wind`, a `relay` for "state estimation", `ch06_autopilot_dynamics`,
`ch10_path_follower`, `ch11_path_manager`, and `ch12_path_planner` nodes. `rqt_robot_monitor` is also opened
to allow easy monitoring of the status of different components. It also opens rviz2 and `uav_plotter`. Again,
keep in mind the scale of rviz. Also, note that the visualized waypoint path drawn 10 meters below its
actual location so that the path segment path is visible.

Before the vehicle will begin moving, you must click the "Start" button on the "Control Panel" in rviz2.
You can then pause the sim by clicking the button again.

The buildings in the world are drawn as red rectangular boxes and the world boundaries are drawn as a green
line. A different version of the world can be created by calling the `regen` service.

After executing the launch file, a goal can be published in two ways:
* Publish to the `/wp_panel/goal_pose` topic (i.e., from command line or via rqt_publisher)
  * If the frame id is "ned", then the goal position will be taken as is
  * If the frame id is "enu", then the altitude will be taken from the `goal_altitude` parameter by adding that
  value to the ground height at the specified (north,east) position. Note that `goal_altitude` can be altered
  from the launch file or via rqt_reconfigure
  * Any other frame_id will be rejected
* Use the RVIZ *2D Goal Pose* publishing capability combined with the waypoint_panel
  * Click on the *2D Goal Pose* button and then click within RVIZ to select a point.
  * This point is then published over the `/goal_pose` topic. The point is defined in the RVIZ fixed frame
  (defaulted to "enu).
  * The waypoint panel reads in this pose and converts it to the "ned" frame
  * When "Publish" is clicked within the waypoint panel, the goal point will be published to
  `/wp_panel/goal_pose`

Replanning will occur in the following situations
* A new goal is received via the `/wp_panel/goal_pose` topic
* A parameter is changed (`goal_altitude` or `planner_type`)
* A service call is made to the `/replan` service (this can be done via rqt)

When publishing a goal point, keep in mind that a plan will not be created if the goal point is in an obstacle,
the goal point lies outside of the city region, or the resulting altitude is less than zero (i.e., "z" in "enu"
must be >= 0). Corresponding error messages appear in the terminal when a plan is not possible.
"""
import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription  # type: ignore
from launch_ros.actions import Node


def generate_launch_description()-> LaunchDescription:
    """Launch chapter 12 demonstration
    """
    # Create the package directory
    uav_launch_dir = get_package_share_directory('uav_launch')

    # Define the urdf file for visualizing the uav
    urdf_file_name = 'fixed_wing_uav.urdf'
    urdf = os.path.join(
        get_package_share_directory('uav_launch'),
        'urdf',
        urdf_file_name)

    # Flag for enabling/disabling use of simulation time instead of wall clock
    use_sim_time = True

    return LaunchDescription([

        ################ Dynamics and kinematics #####################################
        Node(
            package='uav_launch',
            executable='transforms',
            name='transforms',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            arguments=[urdf],
            parameters=[{
                'use_sim_time': use_sim_time
            }]),
        # Dynamics combined with autopilot
        Node(
            package='uav_launch',
            executable='ch06_autopilot_dynamics',
            name='autopilot_dynamic_mav',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch04_wind',
            name='wind',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch07_sensors',
            name="sensors",
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# UAV Control Software #######################################
        # State Estimator
        Node(
            package='topic_tools',
            executable='relay',
            name="state_estimator_relay",
            parameters=[
                {"input_topic": "uav_state"},
                {"output_topic": "uav_state_estimate"},
                {"use_sim_time": use_sim_time}
            ]
        ),

        # Path follower
        Node(
            package='uav_launch',
            executable='ch10_path_follower',
            name='path_follower',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        # Path follower
        Node(
            package='uav_launch',
            executable='ch11_path_manager',
            name='path_manager',
            #emulate_tty=True,
            #output="screen",
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        # Path planner node
        Node(
            package='uav_launch',
            executable='ch12_path_planner',
            name='path_planner',
            remappings=[
            ('/goal_pose', '/wp_panel/goal_pose'),
            ],
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        # World manager
        Node(
            package='uav_launch',
            executable='ch12_world_manager',
            name='world_manager',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# Tools for Interacting with the Sim #########################
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            emulate_tty=True,
            output="screen",
            arguments=['-d', [os.path.join(uav_launch_dir, 'ch11_path_creation.rviz')]],
            parameters=[{
                'use_sim_time': False
            }]
        ),

        Node(
            package='rqt_robot_monitor',
            executable='rqt_robot_monitor',
            name='rqt_robot_monitor',
            parameters=[{
                'use_sim_time': False
            }]
        ),

        Node(
            package='uav_launch',
            executable='diagnotic_aggregator',
            name='diagnotic_aggregator',
            parameters=[{
                'use_sim_time': False
            }]
        ),

        Node(
            package='uav_launch',
            executable='uav_plotter',
            name='uav_plotter',
            parameters=[{
                'use_sim_time': True,
                't_horizon': 100.,
                'plot_sensors': True
            }]
        )
    ])
