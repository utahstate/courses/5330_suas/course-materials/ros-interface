"""Runs the `transforms`, `ch04_wind`, a `relay` for "state estimation", `ch06_autopilot_dynamics`,
and `ch10_path_follower` nodes. `rqt_robot_monitor` is also opened to allow easy monitoring of the status
of different components. It also opens rviz2, rqt_publisher, and `uav_plotter`. Again, keep in mind the scale
of rviz.

Before the vehicle will begin moving, you must click the "Start" button on the "Control Panel" in rviz2. You
can then pause the sim by clicking the button again.

The path segment command can be sent via the `/path_segment` message using the rqt_publisher. Keep in mind
the following restrictions on the `/path_segment` message:
* The *header.frame_id* must be "ned"
* *type* must be "line" or "orbit
* *airspeed* must be greater than $0$
* For a "line" type, the (north, east) (or (x,y)) vector of *line_direction* must have a magnitude greater
than zero
* For an "orbit type,
  * *orbit_radius* must be positive
  * *orbit_direction* must be either "CW" or "CCW"

If any of the above constraints are not met, an error message will be published to the console and the
autopilot command will not be published.
"""
import os

from ament_index_python.packages import get_package_share_directory
from launch import LaunchDescription  # type: ignore
from launch_ros.actions import Node


def generate_launch_description()-> LaunchDescription:
    """Launch chapter 10 demonstration
    """
    # Create the package directory
    uav_launch_dir = get_package_share_directory('uav_launch')

    # Define the urdf file for visualizing the uav
    urdf_file_name = 'fixed_wing_uav.urdf'
    urdf = os.path.join(
        get_package_share_directory('uav_launch'),
        'urdf',
        urdf_file_name)

    # Flag for enabling/disabling use of simulation time instead of wall clock
    use_sim_time = True

    return LaunchDescription([

        ################ Dynamics and kinematics #####################################
        Node(
            package='uav_launch',
            executable='transforms',
            name='transforms',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='robot_state_publisher',
            executable='robot_state_publisher',
            name='robot_state_publisher',
            output='screen',
            arguments=[urdf],
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        # Dynamics combined with autopilot
        Node(
            package='uav_launch',
            executable='ch06_autopilot_dynamics',
            name='autopilot_dynamic_mav',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch04_wind',
            name='wind',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='ch07_sensors',
            name="sensors",
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# UAV Control Software #######################################
        # State Estimator
        Node(
            package='topic_tools',
            executable='relay',
            name="state_estimator_relay",
            parameters=[
                {"input_topic": "uav_state"},
                {"output_topic": "uav_state_estimate"},
                {"use_sim_time": use_sim_time}
            ]
        ),

        # Path follower
        Node(
            package='uav_launch',
            executable='ch10_path_follower',
            name='path_follower',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),

        ################# Tools for Interacting with the Sim #########################
        Node(
            package='rviz2',
            executable='rviz2',
            name='rviz2',
            arguments=['-d', [os.path.join(uav_launch_dir, 'ch04_dynamics.rviz')]],
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='rqt_publisher',
            executable='rqt_publisher',
            name='rqt_publisher',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='rqt_robot_monitor',
            executable='rqt_robot_monitor',
            name='rqt_robot_monitor',
            parameters=[{
                'use_sim_time': False
            }]
        ),
        Node(
            package='uav_launch',
            executable='diagnotic_aggregator',
            name='diagnotic_aggregator',
            parameters=[{
                'use_sim_time': use_sim_time
            }]
        ),
        Node(
            package='uav_launch',
            executable='uav_plotter',
            name='uav_plotter',
            parameters=[{
                'use_sim_time': True,
                't_horizon': 100.,
                'plot_sensors': True
            }]
        )
    ])
