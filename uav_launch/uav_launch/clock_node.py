"""Provides a generic capability for publishing a clock and using that clock to
   run a simulation class that has a take_step() function, called each time the clock is updated

Protocols:
    RosInterface: Generic structure for taking a single simulation time step
    TsClass: Interface to a class that has a timestep property

Types:
    InterfaceConstructor: A function that takes in the time step, the latest time, and a node
                          and produces the simulation class

Classes:

Functions:
    run_clock_node(): Runs a clock and a simulator that has a take_step() function

"""
import threading
import time as pytime
from typing import Any, Callable, Protocol

import rclpy
from rcl_interfaces.msg import ParameterDescriptor
from rclpy.executors import SingleThreadedExecutor
from rclpy.node import Node
from rclpy.time import Time
from rosgraph_msgs.msg import Clock
from uav_interfaces.srv import SetClockParams


class SimulationInterface(Protocol):
    """Defined the interface for a general ros interface
    """
    def take_step(self) -> Time:
        """Used for propagating the simulation one step into the future"""

class TsClass(Protocol):
    """Defines the interface to a class that has a time step property
    """
    ts: float # Time step
    executing: bool # Flag indicating whether or not the time is updating

InterfaceConstructor = Callable[[float, Time, Node], tuple[SimulationInterface,TsClass]]

def run_clock_node(node_name: str, get_ros_interface: InterfaceConstructor, args:Any = None) -> None:
    """ Runs the clock with a simulation interface that has a take_step() function.

    Inputs:
        node_name: Name of the node to be created
        get_ros_interfase: Returns a RosInterface node and structure with the desired time step
        args: The commandline inputs to the start of the node
    """
    # Initialize ros
    rclpy.init(args=args)
    node = rclpy.create_node(node_name=node_name)

    # Create the clock service function (note that loop_period_ms is ignored)
    wall_time_scale = 1. # wall seconds / sim seconds
    def set_clock_params(req: SetClockParams.Request, res: SetClockParams.Response) -> SetClockParams.Response:
        """Stores the sim scale (ratio of sim clock to system clock)

        Args:
            req: Stores the requested time scale
            res: Indicates whether or not a response was successful
        """
        if req.time_scale > 0:
            nonlocal wall_time_scale
            wall_time_scale = 1./req.time_scale # Note that time scale is wall seconds / sim time seconds
            res.success = True
        else:
            res.success = False
            node.get_logger().error("Commanded time scale not positive")
        return res

    # Create the clock adjustment service service (must stay in scope until program finishes - called by executor)
    clock_server = node.create_service(SetClockParams, "set_clock_params",set_clock_params) #pylint: disable=unused-variable

    # Initialize the clock variables
    time_latest = Time()
    clock_msg = Clock()
    clock_msg.clock = time_latest.to_msg()
    pub_clock = node.create_publisher(Clock, "/clock", 1)
    pub_clock.publish(clock_msg)

    # Create the executor
    executor = SingleThreadedExecutor()
    executor.add_node(node)

    # Create a separate thread for processing inputs via ros
    ros_thread = threading.Thread(target=executor.spin, daemon=True)
    ros_thread.start()

    # Initialize the dynamics node
    ros_interface, ts_class = get_ros_interface(0.01, time_latest, node)

    # Wait for simulation to be initialized
    init_time_wait= ts_class.ts # Seconds
    node.declare_parameter(name="initialization_wait_time",
                           value=init_time_wait,
                           descriptor=ParameterDescriptor(description=
                            'Amount of time in seconds this node will wait before starting the simulation'))
    init_time_wait = node.get_parameter("initialization_wait_time").value
    wall_time = pytime.perf_counter()
    desired_wall_time = wall_time + init_time_wait
    while rclpy.ok() and desired_wall_time > pytime.perf_counter():
        executor.spin_once(timeout_sec=max(0.0, desired_wall_time - pytime.perf_counter()))

    # Loop through and publish the clock
    wall_time = pytime.perf_counter()
    pub_warning_time = pytime.perf_counter() # Used to publish a warning when the clock is not exeucint fast enough
    while rclpy.ok():
        # Update the sim
        time_latest = ros_interface.take_step()

        # Update the clock
        clock_msg.clock = time_latest.to_msg()
        pub_clock.publish(clock_msg)

        # Just sleep if the sim is not executing
        if not ts_class.executing:
            pytime.sleep(1.)
            continue

        # Sleep until next publication need to be made
        desired_wall_time = wall_time+ ts_class.ts*wall_time_scale

        sleep_duration = desired_wall_time-pytime.perf_counter()
        if sleep_duration > 0:
            pytime.sleep(sleep_duration)
            wall_time = desired_wall_time
        else: # Don't catch up, just go as fast as possible
            wall_time = pytime.perf_counter()
            if wall_time > pub_warning_time:
                node.get_logger().warn("Simulated clock and dynamics cannot keep up with desired speed")
                pub_warning_time = wall_time + 5.


    # Shutdown the node
    ros_thread.join()
    node.destroy_node()
    rclpy.shutdown()
